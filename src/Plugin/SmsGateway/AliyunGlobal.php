<?php

namespace Drupal\smsaliyun\Plugin\SmsGateway;

use Drupal\Core\Form\FormStateInterface;
use Drupal\sms\Message\SmsDeliveryReport;
use Drupal\sms\Message\SmsMessageReportStatus;
use Drupal\sms\Plugin\SmsGatewayPluginBase;
use Drupal\sms\Message\SmsMessageInterface;
use Drupal\sms\Message\SmsMessageResult;
use AlibabaCloud\Client\AlibabaCloud;
use AlibabaCloud\Client\Exception\ClientException;
use AlibabaCloud\Client\Exception\ServerException;

/**
 * @SmsGateway(
 *    id = "aliyun_global",
 *    label = @Translation("Aliyun Global"),
 *    outgoing_message_max_recipients = 1,
 *    reports_push = TRUE,
 * )
 */
class AliyunGlobal extends SmsGatewayPluginBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'access_key_id' => '',
      'access_key_secret' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $config = $this->getConfiguration();

    $form['aliyun'] = [
      '#type' => 'details',
      '#title' => $this->t('Aliyun'),
      '#open' => TRUE,
    ];
    $form['aliyun']['region_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Region id'),
      '#default_value' => $config['region_id'],
      '#placeholder' => '',
      '#required' => TRUE,
    ];
    $form['aliyun']['access_key_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Access Key Id'),
      '#default_value' => $config['access_key_id'],
      '#placeholder' => '16 bits',
      '#required' => TRUE,
    ];

    $form['aliyun']['access_key_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Access Key Secret'),
      '#default_value' => $config['access_key_secret'],
      '#placeholder' => '30 bits',
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['access_key_id'] = trim($form_state->getValue('access_key_id'));
    $this->configuration['access_key_secret'] = trim($form_state->getValue('access_key_secret'));
    $this->configuration['region_id'] = trim($form_state->getValue('region_id'));
  }

  /*
   * {@inheritdoc}
   */
  public function send(SmsMessageInterface $sms_message) {
    $recipient = $sms_message->getRecipients()[0];
    $SmsMessageResult = new SmsMessageResult();

    $accesskeyid = $this->configuration['access_key_id'];
    $accesskeysecret = $this->configuration['access_key_secret'];
    $region_id = $this->configuration['region_id'];

    $message_id = '';
    AlibabaCloud::accessKeyClient($accesskeyid, $accesskeysecret)
      ->regionId($region_id)
      ->asGlobalClient();
    try {
      $result = AlibabaCloud::rpcRequest()
        ->product('Dysmsapi')
        ->host('sms-intl.ap-southeast-1.aliyuncs.com')//TODO::这里怎么获取到正确的host?
        ->version('2018-05-01')
        ->action('SendMessageToGlobe')
        ->method('POST')
        ->options([
          'query' => [
            "To" => $sms_message->getRecipients()[0],
            // "From" => "1234567890",
            "Message" => $sms_message->getMessage(),
          ],
        ])
        ->request()
        ->toArray();
      if ($result['ResponseCode'] == 'OK') {
        $message_id = $result['MessageId'];
        $status = SmsMessageReportStatus::QUEUED;
        $status_message = "";
      }
      else {
        //错误描述文档：
        //https://help.aliyun.com/document_detail/101346.html?spm=a2c4g.11186623.4.1.2ccd2246o20YpV
        $status = SmsMessageReportStatus::ERROR;
        $status_message = $result['ResponseCode'] . '|' . $result['ResponseDescription'];
      }

    } catch (ClientException $e) {
      $status = SmsMessageReportStatus::ERROR;
      $status_message = $e->getErrorMessage();
    } catch (ServerException $e) {
      $status = SmsMessageReportStatus::ERROR;
      $status_message = $e->getErrorMessage();
//      dpm($e->getErrorMessage(), 'server error');
    }
    $report = new SmsDeliveryReport();
    $report->setRecipient($recipient);
    $report->setMessageId($message_id);
    $report->setStatus($status);
    $report->setStatusMessage($status_message);
    dpm($report);
    $SmsMessageResult->addReport($report);
    return $SmsMessageResult;
  }

}
